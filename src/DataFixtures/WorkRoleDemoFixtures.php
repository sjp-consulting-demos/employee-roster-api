<?php

namespace App\DataFixtures;

use App\Factory\WorkRoleFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class WorkRoleDemoFixtures extends Fixture
{
    public const PRIMARY = 'manager';

    public function load(ObjectManager $manager): void
    {
        $manager = WorkRoleFactory::createOne(['name' => 'Manager', 'slug' =>  'manager']);
        WorkRoleFactory::createOne(['name' => 'Bartender', 'slug' =>  'bartender']);
        WorkRoleFactory::createOne(['name' => 'Chef', 'slug' =>  'chef']);
        WorkRoleFactory::createOne(['name' => 'Waiter', 'slug' =>  'waiter']);

        $this->addReference(self::PRIMARY, $manager);
    }
}
