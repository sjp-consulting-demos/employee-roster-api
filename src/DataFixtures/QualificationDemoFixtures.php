<?php

namespace App\DataFixtures;

use App\Factory\QualificationFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class QualificationDemoFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        QualificationFactory::createOne(['name' => 'Bartending License']);
        QualificationFactory::createOne(['name' => 'Chef License']);
        QualificationFactory::createOne(['name' => 'Manager Qualification']);
    }
}
