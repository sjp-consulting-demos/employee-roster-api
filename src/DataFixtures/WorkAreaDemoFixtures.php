<?php

namespace App\DataFixtures;

use App\Entity\Area;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class WorkAreaDemoFixtures extends Fixture implements FixtureGroupInterface
{

    public static function getGroups(): array
    {
        return ["demo_data"];
    }

    public function load(ObjectManager $manager): void
    {
        /** @todo Use Area Factories */
        $kitchen = new Area();
        $kitchen->setName('Kitchen')
            ->setSlug('kitchen')
            ->setDescription('Kitchen');

        $manager->persist($kitchen);

        $bar = new Area();
        $bar->setName('Main Bar')
            ->setSlug('bar-main')
            ->setDescription('Main Bar');

        $manager->persist($bar);

        $bar_outdoor = new Area();
        $bar_outdoor->setName('Outdoor Bar')
            ->setSlug('bar-outdoor')
            ->setDescription('Outdoor Bar');

        $manager->persist($bar_outdoor);

        $dining = new Area();
        $dining->setName('Main Dining')
            ->setSlug('dining-main')
            ->setDescription('Main Dining area');

        $manager->persist($dining);

        $dining_outdoor = new Area();
        $dining_outdoor->setName('Outdoor Dining')
            ->setSlug('dining-outdoor')
            ->setDescription('Outdoor Dining area');

        $manager->flush();
    }
}
