<?php

namespace App\DataFixtures;

use App\Factory\EmployeeFactory;
use App\Factory\QualificationFactory;
use App\Factory\WorkRoleFactory;
use App\Entity\Employee;
use App\Entity\WorkRole;
use App\Factory\AreaFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class EmployeeDemoFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        EmployeeFactory::createOne([
            'Name' => 'John Doe',
            'email' => 'john.doe@example.com',
            'workRoles' => WorkRoleFactory::findBy(['slug' => 'manager']),
            'qualifications' => QualificationFactory::findBy(['name' => 'Manager Qualification']),
            'workAreas' => AreaFactory::findBy(['slug' => 'bar-main'])
        ]);

        EmployeeFactory::createOne([
            'Name' => 'Jane Doe',
            'email' => 'Jane.doe@example.com',
            'workRoles' => WorkRoleFactory::findBy(['slug' => 'manager']),
            'qualifications' => QualificationFactory::findBy(['name' => 'Manager Qualification']),
            'workAreas' => AreaFactory::findBy(['slug' => 'bar-main'])
        ]);

        EmployeeFactory::createOne([
            'Name' => 'Bob Jane',
            'email' => 'bob.jane@example.com',
            'workRoles' => WorkRoleFactory::findBy(['slug' => 'manager']),
            'qualifications' => QualificationFactory::findBy(['name' => 'Manager Qualification']),
            'workAreas' => AreaFactory::findBy(['slug' => 'bar-main'])
        ]);

        EmployeeFactory::createMany(3, [
            'workRoles' => WorkRoleFactory::findBy(['slug' => 'bartender']),
            'qualifications' => QualificationFactory::findBy(['name' => 'Bartender License']),
            'workAreas' => AreaFactory::findBy(['slug' => 'bar-main'])
        ]);

        EmployeeFactory::createMany(2, [
            'workRoles' => WorkRoleFactory::findBy(['slug' => 'chef'])
        ]);

        EmployeeFactory::createMany(5, [
            'workRoles' => WorkRoleFactory::findBy(['slug' => 'waiter'])
        ]);
    }

    public function getDependencies(): array
    {
        return [
            WorkRoleDemoFixtures::class,
            QualificationDemoFixtures::class
        ];
    }
}
