<?php

namespace App\DataFixtures;

use App\Factory\EmployeeFactory;
use App\Entity\Employee;
use App\Factory\LeaveFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use DateTime;

class LeaveDemoFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        LeaveFactory::createOne([
            'startDate' => new DateTime('2021-12-01'),
            'endDate' => new DateTime('2021-12-31'),
            'type' => 'Recreation',
            'notes' => 'notes',
            'employee' => EmployeeFactory::findBy(['name' => 'John Doe'])[0]
        ]);

        LeaveFactory::createOne([
            'startDate' => new DateTime('2021-12-01'),
            'endDate' => new DateTime('2021-12-31'),
            'type' => 'Recreation',
            'notes' => 'notes',
            'employee' => EmployeeFactory::findBy(['name' => 'Jane Doe'])[0]
        ]);
    }

    public function getDependencies(): array
    {
        return [
            EmployeeDemoFixtures::class
        ];
    }
}
