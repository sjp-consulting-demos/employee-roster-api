<?php

namespace App\Entity;

use App\Repository\EmployeeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=EmployeeRepository::class)
 */
class Employee
{
    /**
     * @Groups("employee")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("employee")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("employee")
     */
    private $profile_image;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("employee")
     */
    private $email;

    /**
     * @ORM\Column(type="integer")
     * @Groups("employee")
     */
    private $rosteredHours;

    /**
     * @ORM\Column(type="integer")
     * @Groups("employee")
     */
    private $rosteredAllowance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("employee")
     */
    private $phoneNumber;

    /**
     * @ORM\ManyToMany(targetEntity=Qualification::class, inversedBy="employees")
     */
    private $qualifications;

    /**
     * @ORM\ManyToMany(targetEntity=WorkRole::class, inversedBy="employees")
     * @Groups("employee")
     */
    private $workRoles;

    /**
     * @ORM\OneToMany(targetEntity=Leave::class, mappedBy="employee", orphanRemoval=true)
     */
    private $leave;

    /**
     * @ORM\OneToMany(targetEntity=Shift::class, mappedBy="employee")
     */
    private $shifts;

    /**
     * @ORM\ManyToMany(targetEntity=Area::class, inversedBy="employees")
     */
    private $workAreas;

    public function __construct()
    {
        $this->qualifications = new ArrayCollection();
        $this->workRoles = new ArrayCollection();
        $this->leave = new ArrayCollection();
        $this->shifts = new ArrayCollection();
        $this->workAreas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProfileImage(): ?string
    {
        return $this->profile_image;
    }

    public function setProfileImage(?string $profile_image): self
    {
        $this->profile_image = $profile_image;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRosteredHours(): ?int
    {
        return $this->rosteredHours;
    }

    public function setRosteredHours(int $rosteredHours): self
    {
        $this->rosteredHours = $rosteredHours;

        return $this;
    }

    public function getRosteredAllowance(): ?int
    {
        return $this->rosteredAllowance;
    }

    public function setRosteredAllowance(int $rosteredAllowance): self
    {
        $this->rosteredAllowance = $rosteredAllowance;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return Collection|Qualification[]
     */
    public function getQualifications(): Collection
    {
        return $this->qualifications;
    }

    public function addQualification(Qualification $qualification): self
    {
        if (!$this->qualifications->contains($qualification)) {
            $this->qualifications[] = $qualification;
        }

        return $this;
    }

    public function removeQualification(Qualification $qualification): self
    {
        $this->qualifications->removeElement($qualification);

        return $this;
    }

    /**
     * @return Collection|WorkRole[]
     */
    public function getWorkRoles(): Collection
    {
        return $this->workRoles;
    }

    public function addWorkRole(WorkRole $workRole): self
    {
        if (!$this->workRoles->contains($workRole)) {
            $this->workRoles[] = $workRole;
        }

        return $this;
    }

    public function removeWorkRole(WorkRole $workRole): self
    {
        $this->workRoles->removeElement($workRole);

        return $this;
    }

    /**
     * @return Collection|Leave[]
     */
    public function getLeave(): Collection
    {
        return $this->leave;
    }

    public function addLeave(Leave $leave): self
    {
        if (!$this->leave->contains($leave)) {
            $this->leave[] = $leave;
            $leave->setEmployee($this);
        }

        return $this;
    }

    public function removeLeave(Leave $leave): self
    {
        if ($this->leave->removeElement($leave)) {
            // set the owning side to null (unless already changed)
            if ($leave->getEmployee() === $this) {
                $leave->setEmployee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Shift[]
     */
    public function getShifts(): Collection
    {
        return $this->shifts;
    }

    public function addShift(Shift $shift): self
    {
        if (!$this->shifts->contains($shift)) {
            $this->shifts[] = $shift;
            $shift->setEmployee($this);
        }

        return $this;
    }

    public function removeShift(Shift $shift): self
    {
        if ($this->shifts->removeElement($shift)) {
            // set the owning side to null (unless already changed)
            if ($shift->getEmployee() === $this) {
                $shift->setEmployee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Area[]
     */
    public function getWorkAreas(): Collection
    {
        return $this->workAreas;
    }

    public function addWorkArea(Area $workArea): self
    {
        if (!$this->workAreas->contains($workArea)) {
            $this->workAreas[] = $workArea;
        }

        return $this;
    }

    public function removeWorkArea(Area $workArea): self
    {
        $this->workAreas->removeElement($workArea);

        return $this;
    }
}
