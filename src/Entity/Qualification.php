<?php

namespace App\Entity;

use App\Repository\QualificationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QualificationRepository::class)
 */
class Qualification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $awardedAt;

    /**
     * @ORM\ManyToMany(targetEntity=Employee::class, mappedBy="qualifications")
     */
    private $employees;

    /**
     * @ORM\ManyToMany(targetEntity=WorkRole::class, inversedBy="qualifications")
     */
    private $workRoles;

    public function __construct()
    {
        $this->employees = new ArrayCollection();
        $this->workRoles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAwardedAt(): ?\DateTimeInterface
    {
        return $this->awardedAt;
    }

    public function setAwardedAt(\DateTimeInterface $awardedAt): self
    {
        $this->awardedAt = $awardedAt;

        return $this;
    }

    /**
     * @return Collection|Employee[]
     */
    public function getEmployees(): Collection
    {
        return $this->employees;
    }

    public function addEmployee(Employee $employee): self
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
            $employee->addQualification($this);
        }

        return $this;
    }

    public function removeEmployee(Employee $employee): self
    {
        if ($this->employees->removeElement($employee)) {
            $employee->removeQualification($this);
        }

        return $this;
    }

    /**
     * @return Collection|WorkRole[]
     */
    public function getWorkRoles(): Collection
    {
        return $this->workRoles;
    }

    public function addWorkRole(WorkRole $workRole): self
    {
        if (!$this->workRoles->contains($workRole)) {
            $this->workRoles[] = $workRole;
        }

        return $this;
    }

    public function removeWorkRole(WorkRole $workRole): self
    {
        $this->workRoles->removeElement($workRole);

        return $this;
    }
}
