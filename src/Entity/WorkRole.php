<?php

namespace App\Entity;

use App\Repository\WorkRoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=WorkRoleRepository::class)
 */
class WorkRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("workRole")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("workRole")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("workRole")
     */
    private $slug;

    /**
     * @ORM\Column(type="text")
     * @Groups("workRole")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=Area::class, inversedBy="workRoles")
     */
    private $workAreas;

    /**
     * @ORM\ManyToMany(targetEntity=Employee::class, mappedBy="workRoles")
     */
    private $employees;

    /**
     * @ORM\OneToMany(targetEntity=Shift::class, mappedBy="role")
     */
    private $shifts;

    /**
     * @ORM\ManyToMany(targetEntity=Qualification::class, mappedBy="workRoles")
     */
    private $qualifications;

    public function __construct()
    {
        $this->workAreas = new ArrayCollection();
        $this->employees = new ArrayCollection();
        $this->shifts = new ArrayCollection();
        $this->qualifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Area[]
     */
    public function getWorkAreas(): Collection
    {
        return $this->workAreas;
    }

    public function addWorkArea(Area $workArea): self
    {
        if (!$this->workAreas->contains($workArea)) {
            $this->workAreas[] = $workArea;
        }

        return $this;
    }

    public function removeWorkArea(Area $workArea): self
    {
        $this->workAreas->removeElement($workArea);

        return $this;
    }

    /**
     * @return Collection|Employee[]
     */
    public function getEmployees(): Collection
    {
        return $this->employees;
    }

    public function addEmployee(Employee $employee): self
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
            $employee->addWorkRole($this);
        }

        return $this;
    }

    public function removeEmployee(Employee $employee): self
    {
        if ($this->employees->removeElement($employee)) {
            $employee->removeWorkRole($this);
        }

        return $this;
    }

    /**
     * @return Collection|Shift[]
     */
    public function getShifts(): Collection
    {
        return $this->shifts;
    }

    public function addShift(Shift $shift): self
    {
        if (!$this->shifts->contains($shift)) {
            $this->shifts[] = $shift;
            $shift->setRole($this);
        }

        return $this;
    }

    public function removeShift(Shift $shift): self
    {
        if ($this->shifts->removeElement($shift)) {
            // set the owning side to null (unless already changed)
            if ($shift->getRole() === $this) {
                $shift->setRole(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Qualification[]
     */
    public function getQualifications(): Collection
    {
        return $this->qualifications;
    }

    public function addQualification(Qualification $qualification): self
    {
        if (!$this->qualifications->contains($qualification)) {
            $this->qualifications[] = $qualification;
            $qualification->addWorkRole($this);
        }

        return $this;
    }

    public function removeQualification(Qualification $qualification): self
    {
        if ($this->qualifications->removeElement($qualification)) {
            $qualification->removeWorkRole($this);
        }

        return $this;
    }
}
