<?php

namespace App\Entity;

use App\Repository\AreaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=AreaRepository::class)
 */
class Area
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("area")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("area")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups("area")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("area")
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity=WorkRole::class, mappedBy="workAreas")
     */
    private $workRoles;

    /**
     * @ORM\OneToMany(targetEntity=Shift::class, mappedBy="area")
     */
    private $shifts;

    /**
     * @ORM\ManyToMany(targetEntity=Employee::class, mappedBy="workAreas")
     */
    private $employees;

    public function __construct()
    {
        $this->workRoles = new ArrayCollection();
        $this->shifts = new ArrayCollection();
        $this->employees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|WorkRole[]
     */
    public function getWorkRoles(): Collection
    {
        return $this->workRoles;
    }

    public function addWorkRole(WorkRole $workRole): self
    {
        if (!$this->workRoles->contains($workRole)) {
            $this->workRoles[] = $workRole;
            $workRole->addWorkArea($this);
        }

        return $this;
    }

    public function removeWorkRole(WorkRole $workRole): self
    {
        if ($this->workRoles->removeElement($workRole)) {
            $workRole->removeWorkRole($this);
        }

        return $this;
    }

    /**
     * @return Collection|Shift[]
     */
    public function getShifts(): Collection
    {
        return $this->shifts;
    }

    public function addShift(Shift $shift): self
    {
        if (!$this->shifts->contains($shift)) {
            $this->shifts[] = $shift;
            $shift->setArea($this);
        }

        return $this;
    }

    public function removeShift(Shift $shift): self
    {
        if ($this->shifts->removeElement($shift)) {
            // set the owning side to null (unless already changed)
            if ($shift->getArea() === $this) {
                $shift->setArea(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Employee[]
     */
    public function getEmployees(): Collection
    {
        return $this->employees;
    }

    public function addEmployee(Employee $employee): self
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
            $employee->addWorkArea($this);
        }

        return $this;
    }

    public function removeEmployee(Employee $employee): self
    {
        if ($this->employees->removeElement($employee)) {
            $employee->removeWorkArea($this);
        }

        return $this;
    }
}
