<?php

namespace App\EmployeeRosterManager;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

interface EmployeeRosterManagerInterface
{
    public function getAvailableEmployees(Request $request): JsonResponse;
}
