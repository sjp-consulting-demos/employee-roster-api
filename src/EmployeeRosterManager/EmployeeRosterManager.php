<?php

namespace App\EmployeeRosterManager;

use App\Entity\Employee;
use App\Repository\AreaRepository;
use App\Repository\EmployeeRepository;
use App\Repository\WorkRoleRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class EmployeeRosterManager implements EmployeeRosterManagerInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EmployeeRepository */
    private $employeeRepository;

    /** @var WorkRoleRepository */
    private $workRoleRepository;

    /** @var AreaRepository */
    private $workAreaRepository;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(
        ManagerRegistry $doctrine,
        EmployeeRepository $employeeRepository,
        WorkRoleRepository $workRoleRepository,
        AreaRepository $workAreaRepository,
        SerializerInterface $serializer
    ) {
        $this->entityManager = $doctrine->getManager();
        $this->employeeRepository = $employeeRepository;
        $this->workRoleRepository = $workRoleRepository;
        $this->workAreaRepository = $workAreaRepository;
        $this->serializer = $serializer;
    }

    public function getAvailableEmployees(Request $request): JsonResponse
    {
        $bestMatchEmployees = [];
        $otherStaffEmployees = [];

        /** @todo Create a query to handle this better */
        // Find employees by matched role
        $workRoles = $this->workRoleRepository->findOneBy(['slug' => $request->query->get('workrole')]);
        $roleEmployees = $workRoles ? $workRoles->getEmployees() : [];

        foreach ($roleEmployees as $employee) {
            $bestMatchEmployees[] = $employee;
        }

        /** @todo Create a query to handle this better */
        // Find other staff by matched area/location
        $workAreas = $this->workAreaRepository->findOneBy(['slug' => $request->query->get('area')]);
        $areaEmployees = $workAreas ? $workAreas->getEmployees() : [];

        foreach ($areaEmployees as $employee) {
            $otherStaffEmployees[] = $employee;
        }

        // Prepare response data
        if (!$bestMatchEmployees) {
            $bestMatchEmployees = ['error' => 'No matching employees for role'];
        } else {
            usort($bestMatchEmployees, function ($a, $b) {
                return $a->getRosteredAllowance() < $b->getRosteredAllowance();
            });
            $bestMatchEmployees = $this->serializer->normalize($bestMatchEmployees, 'json', ['groups' => ['employee', 'workRole']]);
        }

        if (!$otherStaffEmployees) {
            $otherStaffEmployees = ['error' => 'No matching employees for area'];
        } else {
            $otherStaffEmployees = $this->serializer->normalize($otherStaffEmployees, 'json', ['groups' => ['employee', 'area', 'workRole']]);
        }

        // Respond with employees for Role and Areas
        return new JsonResponse([
            'employees_best_match' => $bestMatchEmployees,
            'employees_other_staff' => $otherStaffEmployees
        ], JsonResponse::HTTP_OK);
    }
}
