<?php

namespace App\Controller;

use App\EmployeeRosterManager\EmployeeRosterManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmployeesAvailableController extends AbstractController
{
    /**
     * @Route("api/employees/available", name="employees_available")
     */
    public function index(Request $request, EmployeeRosterManagerInterface $rosterManager): Response
    {
        return $rosterManager->getAvailableEmployees($request);
    }
}
