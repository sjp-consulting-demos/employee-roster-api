<?php

namespace App\Factory;

use App\Entity\Shift;
use App\Repository\ShiftRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Shift>
 *
 * @method static Shift|Proxy createOne(array $attributes = [])
 * @method static Shift[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Shift|Proxy find(object|array|mixed $criteria)
 * @method static Shift|Proxy findOrCreate(array $attributes)
 * @method static Shift|Proxy first(string $sortedField = 'id')
 * @method static Shift|Proxy last(string $sortedField = 'id')
 * @method static Shift|Proxy random(array $attributes = [])
 * @method static Shift|Proxy randomOrCreate(array $attributes = [])
 * @method static Shift[]|Proxy[] all()
 * @method static Shift[]|Proxy[] findBy(array $attributes)
 * @method static Shift[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Shift[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ShiftRepository|RepositoryProxy repository()
 * @method Shift|Proxy create(array|callable $attributes = [])
 */
final class ShiftFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'date' => null, // TODO add DATETIME ORM type manually
            'startTime' => null, // TODO add DATETIME ORM type manually
            'endTime' => null, // TODO add DATETIME ORM type manually
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Shift $shift): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Shift::class;
    }
}
