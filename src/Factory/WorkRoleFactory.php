<?php

namespace App\Factory;

use App\Entity\WorkRole;
use App\Repository\WorkRoleRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<WorkRole>
 *
 * @method static WorkRole|Proxy createOne(array $attributes = [])
 * @method static WorkRole[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static WorkRole|Proxy find(object|array|mixed $criteria)
 * @method static WorkRole|Proxy findOrCreate(array $attributes)
 * @method static WorkRole|Proxy first(string $sortedField = 'id')
 * @method static WorkRole|Proxy last(string $sortedField = 'id')
 * @method static WorkRole|Proxy random(array $attributes = [])
 * @method static WorkRole|Proxy randomOrCreate(array $attributes = [])
 * @method static WorkRole[]|Proxy[] all()
 * @method static WorkRole[]|Proxy[] findBy(array $attributes)
 * @method static WorkRole[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static WorkRole[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static WorkRoleRepository|RepositoryProxy repository()
 * @method WorkRole|Proxy create(array|callable $attributes = [])
 */
final class WorkRoleFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'name' => self::faker()->text(5),
            'slug' => self::faker()->text(5),
            'description' => self::faker()->text(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(WorkRole $workRole): void {})
        ;
    }

    protected static function getClass(): string
    {
        return WorkRole::class;
    }
}
