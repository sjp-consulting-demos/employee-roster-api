<?php

namespace App\Factory;

use App\Entity\Leave;
use App\Repository\LeaveRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Leave>
 *
 * @method static Leave|Proxy createOne(array $attributes = [])
 * @method static Leave[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Leave|Proxy find(object|array|mixed $criteria)
 * @method static Leave|Proxy findOrCreate(array $attributes)
 * @method static Leave|Proxy first(string $sortedField = 'id')
 * @method static Leave|Proxy last(string $sortedField = 'id')
 * @method static Leave|Proxy random(array $attributes = [])
 * @method static Leave|Proxy randomOrCreate(array $attributes = [])
 * @method static Leave[]|Proxy[] all()
 * @method static Leave[]|Proxy[] findBy(array $attributes)
 * @method static Leave[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Leave[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static LeaveRepository|RepositoryProxy repository()
 * @method Leave|Proxy create(array|callable $attributes = [])
 */
final class LeaveFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'type' => self::faker()->text(),
            'notes' => self::faker()->text(),
            'startDate' => null, // TODO add DATETIME ORM type manually
            'endDate' => null, // TODO add DATETIME ORM type manually
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Leave $leave): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Leave::class;
    }
}
