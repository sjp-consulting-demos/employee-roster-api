<?php

namespace App\Factory;

use App\Entity\Area;
use App\Repository\AreaRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Area>
 *
 * @method static Area|Proxy createOne(array $attributes = [])
 * @method static Area[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Area|Proxy find(object|array|mixed $criteria)
 * @method static Area|Proxy findOrCreate(array $attributes)
 * @method static Area|Proxy first(string $sortedField = 'id')
 * @method static Area|Proxy last(string $sortedField = 'id')
 * @method static Area|Proxy random(array $attributes = [])
 * @method static Area|Proxy randomOrCreate(array $attributes = [])
 * @method static Area[]|Proxy[] all()
 * @method static Area[]|Proxy[] findBy(array $attributes)
 * @method static Area[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Area[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static AreaRepository|RepositoryProxy repository()
 * @method Area|Proxy create(array|callable $attributes = [])
 */
final class AreaFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'name' => self::faker()->text(),
            'description' => self::faker()->text(),
            'areaCode' => self::faker()->text(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Area $area): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Area::class;
    }
}
