<?php

namespace App\Repository;

use App\Entity\WorkRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WorkRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkRole[]    findAll()
 * @method WorkRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorkRole::class);
    }

    // /**
    //  * @return WorkRole[] Returns an array of WorkRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WorkRole
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllEmployees()
    {
        // return $this->createQueryBuilder('w')
        //     ->
    }
}
