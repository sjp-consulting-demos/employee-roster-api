# Employee Roster API
A prototype of an API that will return a list of employees available for a given shift at a given work location. The API will return a list of employees with two sections. The first section should show a list of "best match" employees in order of rating, and the second section should show a list of "other staff" employees who may be suitable for the shift but aren't directly associated with the role.

# Read the Docs
Project documentation has been written and can be found here:

* [Technical Specifications](docs/technicalspecs.md)
* [Database Design](docs/databasedesign.md)
* [API Design](docs/apidesign.md)
* [Testing Strategy](docs/testingstrategy.md)
* [Test Results (markdown)](docs/testresults.md)
* [Test Results (html)](docs/testresults.html)
* [Final Thoughts](docs/final.md)

# Quickstart Local Environment
1. Clone repository
2. Install Dependencies: `composer install`
3. Set up environment variable overrides as required in `.env.local` and `.env.test.local` as required
4. Start services with `docker compose up -d`
5. Run migrations with `symfony console doctrine:migrations:migrate`
6. Seed data with `symfony console doctrine:fixtures:load`
7. Start dev server with `symfony server:start`
8. Run tests with `php bin/phpunit`

# Built With
* Symfony
* PHP 8
* MySQL
* Docker

# Access the endpoint
You will be able to access the endpoint using a tool like [Postman](https://postman.com). Once the service is running, use the endpoint by sending a GET request to `http://localhost/employees/best-match` with the following parameters:

```
{
    "workrole": "role_name",
    "workarea": "role_location",
    "shiftdate": "Date",
    "shiftstart": "start time",
    "shiftend": "end time"
}

// Example:
GET api/employees/available?workrole=bartender&workarea=bar-main

```