<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220103084936 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE area (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, area_code VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, profile_image VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, rostered_hours INT NOT NULL, rostered_allowance INT NOT NULL, phone_number VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `leave` (id INT AUTO_INCREMENT NOT NULL, employee_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, notes LONGTEXT NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, INDEX IDX_9BB080D08C03F15C (employee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE qualification (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, awarded_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE qualification_employee (qualification_id INT NOT NULL, employee_id INT NOT NULL, INDEX IDX_BBF527B1A75EE38 (qualification_id), INDEX IDX_BBF527B8C03F15C (employee_id), PRIMARY KEY(qualification_id, employee_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE qualification_work_role (qualification_id INT NOT NULL, work_role_id INT NOT NULL, INDEX IDX_C8FA20BF1A75EE38 (qualification_id), INDEX IDX_C8FA20BF3C89A8E (work_role_id), PRIMARY KEY(qualification_id, work_role_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shift (id INT AUTO_INCREMENT NOT NULL, employee_id INT DEFAULT NULL, area_id INT DEFAULT NULL, role_id INT DEFAULT NULL, date DATETIME NOT NULL, start_time DATETIME NOT NULL, end_time DATETIME NOT NULL, INDEX IDX_A50B3B458C03F15C (employee_id), INDEX IDX_A50B3B45BD0F409C (area_id), INDEX IDX_A50B3B45D60322AC (role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE work_role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE work_role_area (work_role_id INT NOT NULL, area_id INT NOT NULL, INDEX IDX_B23E7A93C89A8E (work_role_id), INDEX IDX_B23E7A9BD0F409C (area_id), PRIMARY KEY(work_role_id, area_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE work_role_employee (work_role_id INT NOT NULL, employee_id INT NOT NULL, INDEX IDX_3791EAA43C89A8E (work_role_id), INDEX IDX_3791EAA48C03F15C (employee_id), PRIMARY KEY(work_role_id, employee_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `leave` ADD CONSTRAINT FK_9BB080D08C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id)');
        $this->addSql('ALTER TABLE qualification_employee ADD CONSTRAINT FK_BBF527B1A75EE38 FOREIGN KEY (qualification_id) REFERENCES qualification (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE qualification_employee ADD CONSTRAINT FK_BBF527B8C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE qualification_work_role ADD CONSTRAINT FK_C8FA20BF1A75EE38 FOREIGN KEY (qualification_id) REFERENCES qualification (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE qualification_work_role ADD CONSTRAINT FK_C8FA20BF3C89A8E FOREIGN KEY (work_role_id) REFERENCES work_role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shift ADD CONSTRAINT FK_A50B3B458C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id)');
        $this->addSql('ALTER TABLE shift ADD CONSTRAINT FK_A50B3B45BD0F409C FOREIGN KEY (area_id) REFERENCES area (id)');
        $this->addSql('ALTER TABLE shift ADD CONSTRAINT FK_A50B3B45D60322AC FOREIGN KEY (role_id) REFERENCES work_role (id)');
        $this->addSql('ALTER TABLE work_role_area ADD CONSTRAINT FK_B23E7A93C89A8E FOREIGN KEY (work_role_id) REFERENCES work_role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE work_role_area ADD CONSTRAINT FK_B23E7A9BD0F409C FOREIGN KEY (area_id) REFERENCES area (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE work_role_employee ADD CONSTRAINT FK_3791EAA43C89A8E FOREIGN KEY (work_role_id) REFERENCES work_role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE work_role_employee ADD CONSTRAINT FK_3791EAA48C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE shift DROP FOREIGN KEY FK_A50B3B45BD0F409C');
        $this->addSql('ALTER TABLE work_role_area DROP FOREIGN KEY FK_B23E7A9BD0F409C');
        $this->addSql('ALTER TABLE `leave` DROP FOREIGN KEY FK_9BB080D08C03F15C');
        $this->addSql('ALTER TABLE qualification_employee DROP FOREIGN KEY FK_BBF527B8C03F15C');
        $this->addSql('ALTER TABLE shift DROP FOREIGN KEY FK_A50B3B458C03F15C');
        $this->addSql('ALTER TABLE work_role_employee DROP FOREIGN KEY FK_3791EAA48C03F15C');
        $this->addSql('ALTER TABLE qualification_employee DROP FOREIGN KEY FK_BBF527B1A75EE38');
        $this->addSql('ALTER TABLE qualification_work_role DROP FOREIGN KEY FK_C8FA20BF1A75EE38');
        $this->addSql('ALTER TABLE qualification_work_role DROP FOREIGN KEY FK_C8FA20BF3C89A8E');
        $this->addSql('ALTER TABLE shift DROP FOREIGN KEY FK_A50B3B45D60322AC');
        $this->addSql('ALTER TABLE work_role_area DROP FOREIGN KEY FK_B23E7A93C89A8E');
        $this->addSql('ALTER TABLE work_role_employee DROP FOREIGN KEY FK_3791EAA43C89A8E');
        $this->addSql('DROP TABLE area');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE `leave`');
        $this->addSql('DROP TABLE qualification');
        $this->addSql('DROP TABLE qualification_employee');
        $this->addSql('DROP TABLE qualification_work_role');
        $this->addSql('DROP TABLE shift');
        $this->addSql('DROP TABLE work_role');
        $this->addSql('DROP TABLE work_role_area');
        $this->addSql('DROP TABLE work_role_employee');
    }
}
