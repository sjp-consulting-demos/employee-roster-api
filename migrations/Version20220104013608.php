<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220104013608 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE employee_qualification (employee_id INT NOT NULL, qualification_id INT NOT NULL, INDEX IDX_C1E084758C03F15C (employee_id), INDEX IDX_C1E084751A75EE38 (qualification_id), PRIMARY KEY(employee_id, qualification_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee_work_role (employee_id INT NOT NULL, work_role_id INT NOT NULL, INDEX IDX_AB3A53108C03F15C (employee_id), INDEX IDX_AB3A53103C89A8E (work_role_id), PRIMARY KEY(employee_id, work_role_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee_leave (id INT AUTO_INCREMENT NOT NULL, employee_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, notes LONGTEXT NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, INDEX IDX_73BD8DA98C03F15C (employee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE employee_qualification ADD CONSTRAINT FK_C1E084758C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_qualification ADD CONSTRAINT FK_C1E084751A75EE38 FOREIGN KEY (qualification_id) REFERENCES qualification (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_work_role ADD CONSTRAINT FK_AB3A53108C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_work_role ADD CONSTRAINT FK_AB3A53103C89A8E FOREIGN KEY (work_role_id) REFERENCES work_role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_leave ADD CONSTRAINT FK_73BD8DA98C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE employee_qualification');
        $this->addSql('DROP TABLE employee_work_role');
        $this->addSql('DROP TABLE employee_leave');
    }
}
