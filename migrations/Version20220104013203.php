<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220104013203 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE `leave`');
        $this->addSql('DROP TABLE qualification_employee');
        $this->addSql('DROP TABLE work_role_employee');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `leave` (id INT AUTO_INCREMENT NOT NULL, employee_id INT DEFAULT NULL, type VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, notes LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, INDEX IDX_9BB080D08C03F15C (employee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE qualification_employee (qualification_id INT NOT NULL, employee_id INT NOT NULL, INDEX IDX_BBF527B1A75EE38 (qualification_id), INDEX IDX_BBF527B8C03F15C (employee_id), PRIMARY KEY(qualification_id, employee_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE work_role_employee (work_role_id INT NOT NULL, employee_id INT NOT NULL, INDEX IDX_3791EAA43C89A8E (work_role_id), INDEX IDX_3791EAA48C03F15C (employee_id), PRIMARY KEY(work_role_id, employee_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE `leave` ADD CONSTRAINT FK_9BB080D08C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id)');
        $this->addSql('ALTER TABLE qualification_employee ADD CONSTRAINT FK_BBF527B1A75EE38 FOREIGN KEY (qualification_id) REFERENCES qualification (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE qualification_employee ADD CONSTRAINT FK_BBF527B8C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE work_role_employee ADD CONSTRAINT FK_3791EAA43C89A8E FOREIGN KEY (work_role_id) REFERENCES work_role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE work_role_employee ADD CONSTRAINT FK_3791EAA48C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
    }
}
