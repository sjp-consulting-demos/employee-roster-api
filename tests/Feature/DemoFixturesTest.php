<?php

namespace App\Tests\Feature;

use App\Entity\Area;
use App\Entity\WorkRole;
use App\Entity\Qualification;
use App\Entity\Employee;
use App\Entity\Leave;
use App\Entity\Shift;
use App\DataFixtures\WorkAreaDemoFixtures;
use App\DataFixtures\WorkRoleDemoFixtures;
use App\DataFixtures\QualificationDemoFixtures;
use App\DataFixtures\EmployeeDemoFixtures;
use App\DataFixtures\LeaveDemoFixtures;
use App\DataFixtures\ShiftDemoFixtures;
use Doctrine\Common\DataFixtures\ReferenceRepository;

class DemoFixturesTest extends DatabaseDependentTestCase
{
    /** @var ReferenceRepository */
    public $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->referenceRepository = new ReferenceRepository($this->entityManager);
    }

    /** @test */
    public function area_fixtures_create_demo_areas(): void
    {
        // SETUP
        $workAreaFixture = new WorkAreaDemoFixtures();
        $workAreaFixture->setReferenceRepository($this->referenceRepository);
        $workAreaRepo = $this->entityManager->getRepository(Area::class);

        // DO SOMETHING
        $workAreaFixture->load($this->entityManager);
        $areaRecord = $workAreaRepo->findOneBy(['slug' => 'bar-main']);
        $allRecords = $workAreaRepo->findAll();

        // ASSERT
        $this->assertEquals('bar-main', $areaRecord->getSlug());
        $this->assertCount(4, $allRecords); // Bar, Kitchen, Dining
    }

    /** @test */
    public function work_role_fixtures_create_demo_work_roles(): void
    {
        // SETUP
        $fixture = new WorkRoleDemoFixtures();
        $fixture->setReferenceRepository($this->referenceRepository);
        $repo = $this->entityManager->getRepository(WorkRole::class);

        // DO SOMETHING
        $fixture->load($this->entityManager);
        $workRoleRecord = $repo->find(1);
        $allRecords = $repo->findAll();

        // ASSERT
        $this->assertEquals('Manager', $workRoleRecord->getName());
        $this->assertEquals('manager', $workRoleRecord->getSlug());
        $this->assertCount(4, $allRecords); // Manager, Bartender, Chef, Waiter
    }

    /** @test */
    public function qualification_fixtures_create_demo_qualifications(): void
    {
        // SETUP
        $fixture = new QualificationDemoFixtures();
        $fixture->setReferenceRepository($this->referenceRepository);
        $repo = $this->entityManager->getRepository(Qualification::class);

        // DO SOMETHING
        $fixture->load($this->entityManager);
        $qualificationRecord = $repo->find(1);
        $allRecords = $repo->findAll();

        // ASSERT
        $this->assertEquals('Bartending License', $qualificationRecord->getName());
        $this->assertCount(3, $allRecords); // Bartending License, Chef License
    }

    /** @test */
    public function employee_fixtures_create_demo_employees(): void
    {
        // SETUP
        $workRoleFixture = new WorkRoleDemoFixtures();
        $workRoleFixture->setReferenceRepository($this->referenceRepository);
        $workRoleFixture->load($this->entityManager);

        $qualificationFixture = new QualificationDemoFixtures();
        $qualificationFixture->setReferenceRepository($this->referenceRepository);
        $qualificationFixture->load($this->entityManager);

        $fixture = new EmployeeDemoFixtures();
        $fixture->setReferenceRepository($this->referenceRepository);
        $repo = $this->entityManager->getRepository(Employee::class);
        $workRolesRepo = $this->entityManager->getRepository(WorkRole::class);
        $qualificationsRepo = $this->entityManager->getRepository(Qualification::class);

        // DO SOMETHING
        $fixture->load($this->entityManager);
        $employeeRecord = $repo->find(1);

        // Force attach available roles and qualifications
        // TODO: investigate why this is needed when running full testsuite.
        // Works in isolation with php bin/phpunit tests/Feature/DemoFixturesTest.php --filter employee_fixtures_create_demo_employees
        ////
        // $workRole = $workRolesRepo->find(1);
        // $qualification = $qualificationsRepo->find(3);
        // $employeeRecord->addWorkRole($workRole);
        // $employeeRecord->addQualification($qualification);
        // $this->entityManager->flush();
        ////

        $allRecords = $repo->findAll();

        // ASSERT
        $this->assertEquals('John Doe', $employeeRecord->getName());
        $this->assertEquals('Manager', $employeeRecord->getWorkRoles()[0]->getName());
        $this->assertEquals('Manager Qualification', $employeeRecord->getQualifications()[0]->getName());
        $this->assertCount(13, $allRecords);
    }

    /** @test */
    public function leave_fixtures_create_demo_fixtures(): void
    {
        // SETUP
        $employeeFixtures = new EmployeeDemoFixtures();
        $employeeFixtures->setReferenceRepository($this->referenceRepository);
        $employeeFixtures->load($this->entityManager);

        $fixture = new LeaveDemoFixtures();
        $fixture->setReferenceRepository($this->referenceRepository);
        $repo = $this->entityManager->getRepository(Leave::class);

        // DO SOMETHING
        $fixture->load($this->entityManager);
        $leaveRecord = $repo->find(1);
        $allRecords = $repo->findAll();

        // ASSERT
        $this->assertEquals('Recreation', $leaveRecord->getType());
        $this->assertEquals('John Doe', $leaveRecord->getEmployee()->getName());
        $this->assertCount(2, $allRecords);
    }

    /** @test */
    public function shift_fixtures_create_demo_shifts(): void
    {
        // SETUP
        // $fixture = new ShiftDemoFixtures();
        // $fixture->setReferenceRepository($this->referenceRepository);
        // $repo = $this->entityManager->getRepository(Shift::class);

        // // DO SOMETHING
        // $fixture->load($this->entityManager);
        // $shiftRecord = $repo->find(1);
        // $allRecords = $repo->findAll();

        // // ASSERT
        // $this->assertEquals('John Doe', $shiftRecord->getEmployee()->getName());
        // $this->assertCount(5, $allRecords);
    }
}
