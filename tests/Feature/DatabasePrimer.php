<?php

namespace App\Tests\Feature;

use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\HttpKernel\KernelInterface;

class DatabasePrimer
{
    public static function prime(KernelInterface $kernel): void
    {
        if ($kernel->getEnvironment() !== 'test') {
            throw new \LogicException('Primer must be executed in the test environment.');
        }

        // Get the entity manager and run schema update tool using entity data
        $entityManager = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $metadata = $entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool($entityManager);
        $schemaTool->updateSchema($metadata);
    }
}
