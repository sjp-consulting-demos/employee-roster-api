<?php

namespace App\Tests\Feature\Entities;

use App\Entity\Area;
use App\Tests\Feature\DatabaseDependentTestCase;

class AreaTest extends DatabaseDependentTestCase
{
    /** @test */
    public function an_area_record_can_be_created_in_the_database(): void
    {
        // SETUP
        $area = new Area();
        $area->setName('Main')
            ->setSlug('main')
            ->setDescription('The main work area');

        // DO SOMETHING
        $this->entityManager->persist($area);
        $this->entityManager->flush();
        $areaRecord = $this->entityManager->getRepository(Area::class)->findOneBy(['slug' => 'main']);

        // ASSERTIONS
        $this->assertEquals('Main', $areaRecord->getName());
        $this->assertEquals('main', $areaRecord->getSlug());
        $this->assertEquals('The main work area', $areaRecord->getDescription());
    }

    /** @test */
    public function an_area_record_can_be_updated_in_the_database(): void
    {
        // SETUP
        $area = new Area();
        $area->setName('Main')
            ->setSlug('main')
            ->setDescription('The main work area');
        $this->entityManager->persist($area);
        $this->entityManager->flush();
        $areaRecord = $this->entityManager->getRepository(Area::class)->findOneBy(['slug' => 'main']);

        // DO SOMETHING
        // Update description and save
        $areaRecord->setDescription('A new description');
        $this->entityManager->flush();
        $this->entityManager->refresh($areaRecord);

        // ASSERTIONS
        $this->assertEquals('Main', $areaRecord->getName());
        $this->assertEquals('main', $areaRecord->getSlug());
        $this->assertEquals('A new description', $areaRecord->getDescription());
    }

    /** @test */
    public function an_area_record_can_be_deleted_in_the_database(): void
    {
        // SETUP
        // Create area
        $area = new Area();
        $area->setName('Main')
            ->setSlug('main')
            ->setDescription('The main work area');
        $this->entityManager->persist($area);
        $this->entityManager->flush();

        // Fetch and assert record was created
        $areaRecord = $this->entityManager->getRepository(Area::class)->findOneBy(['slug' => 'main']);

        $this->assertEquals('Main', $areaRecord->getName());
        $this->assertEquals('main', $areaRecord->getSlug());
        $this->assertEquals('The main work area', $areaRecord->getDescription());

        // DO SOMETHING
        // Delete the record
        $this->entityManager->remove($areaRecord);
        $this->entityManager->flush();

        // Query for non existing record
        $areaRecord = $this->entityManager->getRepository(Area::class)->findOneBy(['slug' => 'main']);

        // ASSERTIONS
        $this->assertEmpty($areaRecord);
    }
}
