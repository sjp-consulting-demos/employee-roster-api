<?php

namespace App\Tests\Feature\Entities;

use App\Entity\Employee;
use App\Entity\Qualification;
use App\Tests\Feature\DatabaseDependentTestCase;
use DateTime;

class QualificationTest extends DatabaseDependentTestCase
{
    /** @test */
    public function a_qualification_record_can_be_created_in_the_database(): void
    {
        // SETUP
        $qualification = new Qualification();
        $awardedAt = new DateTime('2021-06-01');
        $qualification->setName('Bartender License')
            ->setDescription('License for Bartending')
            ->setAwardedAt($awardedAt);

        // DO SOMETHING
        $this->entityManager->persist($qualification);
        $this->entityManager->flush();
        $qualificationRecord = $this->entityManager->getRepository(Qualification::class)->find($qualification->getid());

        // ASSERT
        $this->assertEquals('Bartender License', $qualificationRecord->getName());
        $this->assertEquals('License for Bartending', $qualificationRecord->getDescription());
        $this->assertEquals($awardedAt, $qualificationRecord->getAwardedAt());
    }

    /** @test */
    public function a_qualification_record_can_be_updated_in_the_database(): void
    {
        // SETUP
        $qualification = new Qualification();
        $awardedAt = new DateTime('2021-06-01');
        $qualification->setName('Bartender License')
            ->setDescription('License for Bartending')
            ->setAwardedAt($awardedAt);
        $this->entityManager->persist($qualification);
        $this->entityManager->flush();
        $qualificationRecord = $this->entityManager->getRepository(Qualification::class)->find($qualification->getid());

        // DO SOMETHING
        // Update description and save
        $qualificationRecord->setDescription('A new description');
        $this->entityManager->flush();
        $this->entityManager->refresh($qualificationRecord);

        // ASSERT
        $this->assertEquals('Bartender License', $qualificationRecord->getName());
        $this->assertEquals('A new description', $qualificationRecord->getDescription());
        $this->assertEquals($awardedAt, $qualificationRecord->getAwardedAt());
    }

    /** @test */
    public function a_qualification_record_can_be_deleted_in_the_database(): void
    {
        // SETUP
        $qualification = new Qualification();
        $awardedAt = new DateTime('2021-06-01');
        $qualification->setName('Bartender License')
            ->setDescription('License for Bartending')
            ->setAwardedAt($awardedAt);
        $this->entityManager->persist($qualification);
        $this->entityManager->flush();

        // Fetch and assert record was created
        $qualificationRecord = $this->entityManager->getRepository(Qualification::class)->findOneBy(['name' => 'Bartender License']);

        $this->assertEquals('Bartender License', $qualificationRecord->getName());
        $this->assertEquals('License for Bartending', $qualificationRecord->getDescription());
        $this->assertEquals($awardedAt, $qualificationRecord->getAwardedAt());

        // DO SOMETHING
        // Delete the record
        $this->entityManager->remove($qualificationRecord);
        $this->entityManager->flush();

        // Query for non existing record
        $qualificationRecord = $this->entityManager->getRepository(Qualification::class)->findOneBy(['name' => 'Bartender License']);

        // ASSERT
        $this->assertEmpty($qualificationRecord);
    }

    /** @test */
    public function employees_can_be_attached_and_retrieved_from_a_qualification(): void
    {
        // SETUP
        $employee = new Employee();
        $employee->setName('John Doe')
            ->setEmail('john.doe@example.com')
            ->setPhoneNumber('0412345678')
            ->setProfileImage('/profiles/images/john.jpg')
            ->setRosteredHours(0)
            ->setRosteredAllowance(38);

        $qualification = new Qualification();
        $awardedAt = new DateTime('2021-06-01');
        $qualification->setName('Bartender License')
            ->setDescription('License for Bartending')
            ->setAwardedAt($awardedAt)
            ->addEmployee($employee);

        $this->entityManager->persist($employee);
        $this->entityManager->persist($qualification);
        $this->entityManager->flush();

        // DO SOMETHING
        $qualificationRecord = $this->entityManager->getRepository(Qualification::class)->findOneBy(['name' => 'Bartender License']);

        // ASSERT
        $this->assertEquals('John Doe', $qualificationRecord->getEmployees()[0]->getName());
    }

    /** @test */
    public function an_employee_can_be_detached_from_a_qualification(): void
    {
        // SETUP
        $employee = new Employee();
        $employee->setName('John Doe')
            ->setEmail('john.doe@example.com')
            ->setPhoneNumber('0412345678')
            ->setProfileImage('/profiles/images/john.jpg')
            ->setRosteredHours(0)
            ->setRosteredAllowance(38);

        $this->entityManager->persist($employee);

        $qualification = new Qualification();
        $awardedAt = new DateTime('2021-06-01');
        $qualification->setName('Bartender License')
            ->setDescription('License for Bartending')
            ->setAwardedAt($awardedAt);
        $this->entityManager->persist($qualification);
        $this->entityManager->flush();

        // DO SOMETHING
        $qualificationRecord = $this->entityManager->getRepository(Qualification::class)->findOneBy(['name' => 'Bartender License']);
        $qualificationRecord->removeEmployee($employee);
        $this->entityManager->flush();

        // ASSERT
        $this->assertEmpty($qualificationRecord->getEmployees());
        $this->assertCount(0, $qualificationRecord->getEmployees());
    }
}
