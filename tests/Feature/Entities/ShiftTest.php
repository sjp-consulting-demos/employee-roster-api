<?php

namespace App\Tests\Feature\Entities;

use App\Entity\Area;
use App\Entity\Employee;
use App\Entity\Shift;
use App\Entity\WorkRole;
use App\Tests\Feature\DatabaseDependentTestCase;
use DateTime;

class ShiftTest extends DatabaseDependentTestCase
{
    /** @var Area */
    public $area;

    /** @var WorkRole */
    public $workRole;

    /** @var Employee */
    public $employee;

    public function setUp(): void
    {
        parent::setUp();

        // @TODO: Use factories
        // Create an Area
        $this->area = new Area();
        $this->area->setName('Main Bar')
            ->setSlug('main')
            ->setDescription('Main Bar');

        $this->entityManager->persist($this->area);

        // Create an employee
        $this->employee = new Employee();
        $this->employee->setName('John Doe')
            ->setEmail('john.doe@example.com')
            ->setPhoneNumber('0412345678')
            ->setRosteredHours(8)
            ->setRosteredAllowance(30);

        $this->entityManager->persist($this->employee);

        // Create a role
        $this->workRole = new WorkRole();
        $this->workRole->setName('Bartender')
            ->setSlug('bartender')
            ->setDescription('Front of House');

        $this->entityManager->persist($this->workRole);
    }

    /** @test */
    public function a_shift_record_can_be_created_in_the_database(): void
    {
        // SETUP
        // Create shift
        $shift = new Shift();
        $shift_date = new DateTime('2020-12-01');
        $shift_start = new DateTime('2021-12-01 08:00');
        $shift_end = new DateTime('2021-12-01 15:00');
        $shift->setDate($shift_date)
            ->setStartTime($shift_start)
            ->setEndTime($shift_end)
            ->setEmployee($this->employee)
            ->setArea($this->area)
            ->setRole($this->workRole);

        // DO SOMETHING
        $this->entityManager->persist($shift);
        $this->entityManager->flush();

        $shiftRecord = $this->entityManager->getRepository(Shift::class)->find($shift->getId());

        // ASSERT
        $this->assertEquals('John Doe', $shiftRecord->getEmployee()->getName());
        $this->assertEquals($shift_date, $shiftRecord->getDate());
        $this->assertEquals($shift_start, $shiftRecord->getStartTime());
        $this->assertEquals($shift_end, $shiftRecord->getEndTime());
        $this->assertEquals('Bartender', $shiftRecord->getRole()->getName());
        $this->assertEquals('Main Bar', $shiftRecord->getArea()->getName());
    }

    /** @test */
    public function a_shift_record_can_be_updated_in_the_database(): void
    {
        // SETUP
        // Create shift
        $shift = new Shift();
        $shift_date = new DateTime('2020-12-01');
        $shift_start = new DateTime('2021-12-01 08:00');
        $shift_end = new DateTime('2021-12-01 15:00');
        $shift->setDate($shift_date)
            ->setStartTime($shift_start)
            ->setEndTime($shift_end)
            ->setEmployee($this->employee)
            ->setArea($this->area)
            ->setRole($this->workRole);

        // Create new employee
        $employee = new Employee();
        $employee->setName('Jane Doe')
            ->setEmail('jane.doe@example.com')
            ->setPhoneNumber('0412345679')
            ->setRosteredHours(8)
            ->setRosteredAllowance(30);

        $this->entityManager->persist($shift);
        $this->entityManager->persist($employee);
        $this->entityManager->flush();

        // DO SOMETHING
        $shiftRecord = $this->entityManager->getRepository(Shift::class)->find($shift->getId());
        $shiftRecord->setEmployee($employee);
        $this->entityManager->flush();
        $this->entityManager->refresh($shiftRecord);

        // ASSERT
        $this->assertEquals('Jane Doe', $shiftRecord->getEmployee()->getName());
        $this->assertEquals($shift_date, $shiftRecord->getDate());
        $this->assertEquals($shift_start, $shiftRecord->getStartTime());
        $this->assertEquals($shift_end, $shiftRecord->getEndTime());
        $this->assertEquals('Bartender', $shiftRecord->getRole()->getName());
        $this->assertEquals('Main Bar', $shiftRecord->getArea()->getName());
    }

    /** @test */
    public function a_shift_record_can_be_deleted_in_the_database(): void
    {
        // SETUP
        // Create shift
        $shift = new Shift();
        $shift_date = new DateTime('2020-12-01');
        $shift_start = new DateTime('2021-12-01 08:00');
        $shift_end = new DateTime('2021-12-01 15:00');
        $shift->setDate($shift_date)
            ->setStartTime($shift_start)
            ->setEndTime($shift_end)
            ->setEmployee($this->employee)
            ->setArea($this->area)
            ->setRole($this->workRole);

        $this->entityManager->persist($shift);
        $this->entityManager->flush();

        // Assert successfully created
        $shiftRecord = $this->entityManager->getRepository(Shift::class)->findOneBy(['startTime' => $shift_start]);
        $this->assertEquals('John Doe', $shiftRecord->getEmployee()->getName());
        $this->assertEquals($shift_date, $shiftRecord->getDate());
        $this->assertEquals($shift_start, $shiftRecord->getStartTime());
        $this->assertEquals($shift_end, $shiftRecord->getEndTime());
        $this->assertEquals('Bartender', $shiftRecord->getRole()->getName());
        $this->assertEquals('Main Bar', $shiftRecord->getArea()->getName());

        // DO SOMETHING
        $this->entityManager->remove($shiftRecord);
        $this->entityManager->flush();

        // Query for non existing record
        $shiftRecord = $this->entityManager->getRepository(Shift::class)->findOneBy(['startTime' => $shift_start]);

        // ASSERT
        $this->assertEmpty($shiftRecord);
    }
}
