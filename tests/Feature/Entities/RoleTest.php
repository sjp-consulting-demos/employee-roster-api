<?php

namespace App\Tests\Feature\Entities;

use App\Entity\WorkRole;
use App\Entity\Area;
use App\Entity\Employee;
use App\Tests\Feature\DatabaseDependentTestCase;

class RoleTest extends DatabaseDependentTestCase
{
    public function createWorkRole(): WorkRole
    {
        $role = new WorkRole();
        $role->setName('Bartender')
            ->setDescription('Front of House Bar Staff')
            ->setSlug('bartender');

        return $role;
    }

    /** @test */
    public function a_work_role_record_can_be_created_in_the_database(): void
    {
        // SETUP
        $role = $this->createWorkRole();

        // DO SOMETHING
        $this->entityManager->persist($role);
        $this->entityManager->flush();
        $roleRecord = $this->entityManager->getRepository(WorkRole::class)->find($role->getid());

        // ASSERT
        $this->assertEquals('Bartender', $roleRecord->getName());
        $this->assertEquals('Front of House Bar Staff', $roleRecord->getDescription());
        $this->assertEquals('bartender', $roleRecord->getSlug());
    }

    /** @test */
    public function a_work_role_record_can_be_updated_in_the_database(): void
    {
        // SETUP
        $role = $this->createWorkRole();

        $this->entityManager->persist($role);
        $this->entityManager->flush();
        $roleRecord = $this->entityManager->getRepository(WorkRole::class)->find($role->getid());

        // DO SOMETHING
        // Update description and save
        $roleRecord->setDescription('A new description');
        $this->entityManager->flush();
        $this->entityManager->refresh($roleRecord);

        // ASSERT
        $this->assertEquals('Bartender', $roleRecord->getName());
        $this->assertEquals('A new description', $roleRecord->getDescription());
        $this->assertEquals('bartender', $roleRecord->getSlug());
    }

    /** @test */
    public function a_work_role_record_can_be_deleted_in_the_database(): void
    {
        // SETUP
        $role = $this->createWorkRole();

        $this->entityManager->persist($role);
        $this->entityManager->flush();

        // Fetch record and assert it was created
        $roleRecord = $this->entityManager->getRepository(WorkRole::class)->find($role->getid());

        $this->assertEquals('Bartender', $roleRecord->getName());
        $this->assertEquals('Front of House Bar Staff', $roleRecord->getDescription());

        // DO SOMETHING
        // Delete the record
        $this->entityManager->remove($roleRecord);
        $this->entityManager->flush();

        // Query for non existing record
        $roleRecord = $this->entityManager->getRepository(WorkRole::class)->findOneBy(['name' => 'Bartender']);

        // ASSERT
        $this->assertEmpty($roleRecord);
    }

    /** @test */
    public function an_employee_can_be_attached_to_a_work_role(): void
    {
        // SETUP
        $employee = new Employee();
        $employee->setName('John Doe')
            ->setEmail('john.doe@example.com')
            ->setPhoneNumber('0412345678')
            ->setProfileImage('/profiles/images/john.jpg')
            ->setRosteredHours(0)
            ->setRosteredAllowance(38);

        $role = $this->createWorkRole();
        $role->addEmployee($employee);

        $this->entityManager->persist($role);
        $this->entityManager->persist($employee);
        $this->entityManager->flush();

        // DO SOMETHING
        $roleRecord = $this->entityManager->getRepository(WorkRole::class)->find($role->getid());

        // ASSERT
        $this->assertEquals('John Doe', $roleRecord->getEmployees()[0]->getName());
    }

    /** @test */
    public function an_employee_can_be_detached_from_a_work_role(): void
    {
        // SETUP
        $employee = new Employee();
        $employee->setName('John Doe')
            ->setEmail('john.doe@example.com')
            ->setPhoneNumber('0412345678')
            ->setProfileImage('/profiles/images/john.jpg')
            ->setRosteredHours(0)
            ->setRosteredAllowance(38);

        $role = $this->createWorkRole();
        $role->addEmployee($employee);

        $this->entityManager->persist($role);
        $this->entityManager->persist($employee);
        $this->entityManager->flush();

        // DO SOMETHING
        $roleRecord = $this->entityManager->getRepository(WorkRole::class)->find($role->getid());
        $roleRecord->removeEmployee($employee);
        $this->entityManager->flush();

        // ASSERT
        $this->assertEmpty($roleRecord->getEmployees());
        $this->assertCount(0, $roleRecord->getEmployees());
    }

    /** @test */
    public function a_work_area_can_be_attached_to_a_work_role(): void
    {
        // SETUP
        $area = new Area();
        $area->setName('Main')
            ->setSlug('main')
            ->setDescription('The main work area');

        $role = $this->createWorkRole();

        // DO SOMETHING
        $role->addWorkArea($area);
        $this->entityManager->persist($area);
        $this->entityManager->persist($role);
        $this->entityManager->flush();
        $roleRecord = $this->entityManager->getRepository(WorkRole::class)->find($role->getid());

        // ASSERT
        $this->assertEquals('Main', $roleRecord->getWorkAreas()[0]->getName());
    }

    /** @test */
    public function a_work_area_can_be_detached_from_a_work_role(): void
    {
        // SETUP
        $area = new Area();
        $area->setName('Main')
            ->setSlug('main')
            ->setDescription('The main work area');

        $role = $this->createWorkRole();
        $role->addWorkArea($area);

        $this->entityManager->persist($area);
        $this->entityManager->persist($role);
        $this->entityManager->flush();

        $this->assertEquals('Main', $role->getWorkAreas()[0]->getName());

        // DO SOMETHING
        $roleRecord = $this->entityManager->getRepository(WorkRole::class)->find($role->getid());
        $roleRecord->removeWorkArea($area);
        $this->entityManager->flush();

        // ASSERT
        $this->assertEmpty($roleRecord->getWorkAreas());
        $this->assertCount(0, $roleRecord->getWorkAreas());
    }
}
