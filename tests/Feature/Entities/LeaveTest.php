<?php

namespace App\Tests\Feature\Entities;

use App\Entity\Leave;
use App\Entity\Employee;
use App\Tests\Feature\DatabaseDependentTestCase;
use DateTime;

class LeaveTest extends DatabaseDependentTestCase
{
    /** @test */
    public function a_leave_record_can_be_created_in_the_database(): void
    {
        // SETUP
        $leave = new Leave();
        $startDate = new DateTime('2020-11-01');
        $endDate = new DateTime('2021-12-01');
        $leave->setType('Recreation')
            ->setStartDate($startDate)
            ->setEndDate($endDate)
            ->setNotes('Christmas Break leave');

        // DO SOMETHING
        $this->entityManager->persist($leave);
        $this->entityManager->flush();
        $leaveRecord = $this->entityManager->getRepository(Leave::class)->find($leave->getId());

        // ASSERTIONS
        $this->assertEquals('Recreation', $leaveRecord->getType());
        $this->assertEquals($startDate, $leaveRecord->getStartDate());
        $this->assertEquals($endDate, $leaveRecord->getEndDate());
        $this->assertEquals('Christmas Break leave', $leaveRecord->getNotes());
    }

    /** @test */
    public function a_leave_record_can_be_updated_in_the_database(): void
    {
        // SETUP
        $leave = new Leave();
        $startDate = new DateTime('2020-11-01');
        $endDate = new DateTime('2021-12-01');
        $leave->setType('Recreation')
            ->setStartDate($startDate)
            ->setEndDate($endDate)
            ->setNotes('Christmas Break leave');

        $this->entityManager->persist($leave);
        $this->entityManager->flush();
        $leaveRecord = $this->entityManager->getRepository(Leave::class)->find($leave->getId());

        // DO SOMETHING
        // Update description and save
        $leaveRecord->setNotes('Updated Notes');
        $this->entityManager->flush();
        $this->entityManager->refresh($leaveRecord);

        // ASSERTIONS
        $this->assertEquals('Recreation', $leaveRecord->getType());
        $this->assertEquals($startDate, $leaveRecord->getStartDate());
        $this->assertEquals($endDate, $leaveRecord->getEndDate());
        $this->assertEquals('Updated Notes', $leaveRecord->getNotes());
    }

    /** @test */
    public function a_leave_record_can_be_deleted_in_the_database(): void
    {
        // SETUP
        // Create leave
        $leave = new Leave();
        $startDate = new DateTime('2020-11-01');
        $endDate = new DateTime('2021-12-01');
        $leave->setType('Recreation')
            ->setStartDate($startDate)
            ->setEndDate($endDate)
            ->setNotes('Christmas Break leave');

        $this->entityManager->persist($leave);
        $this->entityManager->flush();

        // Fetch and assert record was created
        $leaveRecord = $this->entityManager->getRepository(Leave::class)->find($leave->getId());

        $this->assertEquals('Recreation', $leaveRecord->getType());
        $this->assertEquals($startDate, $leaveRecord->getStartDate());
        $this->assertEquals($endDate, $leaveRecord->getEndDate());
        $this->assertEquals('Christmas Break leave', $leaveRecord->getNotes());

        // DO SOMETHING
        // Delete the record
        $this->entityManager->remove($leaveRecord);
        $this->entityManager->flush();

        // Query for non existing record
        $leaveRecord = $this->entityManager->getRepository(Leave::class)->findOneBy(['type' => 'Recreation']);

        // ASSERTIONS
        $this->assertEmpty($leaveRecord);
    }

    /** @test */
    public function an_employee_can_be_attached_and_retrieved(): void
    {
        // SETUP
        // Create leave
        $leave = new Leave();
        $startDate = new DateTime('2020-11-01');
        $endDate = new DateTime('2021-12-01');
        $leave->setType('Recreation')
            ->setStartDate($startDate)
            ->setEndDate($endDate)
            ->setNotes('Christmas Break leave');

        // Create employee
        $employee = new Employee();
        $employee->setName('John Doe')
            ->setEmail('john.doe@example.com')
            ->setRosteredHours(0)
            ->setRosteredAllowance(38);

        // Save data
        $this->entityManager->persist($leave);
        $this->entityManager->persist($employee);
        $this->entityManager->flush();

        // DO SOMETHING
        $leaveRecord = $this->entityManager->getRepository(Leave::class)->find($leave->getId());
        $leaveRecord->setEmployee($employee);
        $this->entityManager->flush();
        $this->entityManager->refresh($leaveRecord);

        // ASSERT
        $this->assertEquals('john.doe@example.com', $leaveRecord->getEmployee()->getEmail());
    }
}
