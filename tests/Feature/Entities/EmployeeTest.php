<?php

namespace App\Tests\Feature\Entities;

use App\Entity\Employee;
use App\Entity\WorkRole;
use App\Entity\Leave;
use App\Entity\Qualification;
use App\Entity\Shift;
use App\Tests\Feature\DatabaseDependentTestCase;
use DateTime;

class EmployeeTest extends DatabaseDependentTestCase
{
    public function createEmployee(): Employee
    {
        $employee = new Employee();
        $employee->setName('John Doe')
            ->setEmail('john.doe@example.com')
            ->setPhoneNumber('0412345678')
            ->setProfileImage('/profiles/images/john.jpg')
            ->setRosteredHours(0)
            ->setRosteredAllowance(38);

        return $employee;
    }

    /** @test */
    public function an_employee_record_can_be_created_in_the_database(): void
    {
        // SETUP
        $employee = $this->createEmployee();

        // DO SOMETHING
        $this->entityManager->persist($employee);
        $this->entityManager->flush();
        $employeeRecord = $this->entityManager->getRepository(Employee::class)->find($employee->getId());

        // ASSERT DOMINANCE
        $this->assertEquals('John Doe', $employeeRecord->getName());
        $this->assertEquals('john.doe@example.com', $employeeRecord->getEmail());
        $this->assertEquals('0412345678', $employeeRecord->getPhoneNumber());
        $this->assertEquals('/profiles/images/john.jpg', $employeeRecord->getProfileImage());
        $this->assertEquals(0, $employeeRecord->getRosteredHours());
        $this->assertEquals(38, $employeeRecord->getRosteredAllowance());
    }

    /** @test */
    public function an_employee_record_can_be_updated_in_the_database(): void
    {
        // SETUP
        $employee = $this->createEmployee();

        $this->entityManager->persist($employee);
        $this->entityManager->flush();

        $employeeRecord = $this->entityManager->getRepository(Employee::class)->find($employee->getId());

        // DO SOMETHING
        $employeeRecord->setRosteredHours(8)
            ->setRosteredAllowance(30);
        $this->entityManager->flush();
        $this->entityManager->refresh($employeeRecord);

        // ASSERT
        $this->assertEquals('John Doe', $employeeRecord->getName());
        $this->assertEquals('john.doe@example.com', $employeeRecord->getEmail());
        $this->assertEquals('0412345678', $employeeRecord->getPhoneNumber());
        $this->assertEquals('/profiles/images/john.jpg', $employeeRecord->getProfileImage());
        $this->assertEquals(8, $employeeRecord->getRosteredHours());
        $this->assertEquals(30, $employeeRecord->getRosteredAllowance());
    }

    /** @test */
    public function an_employee_record_can_be_deleted_in_the_database(): void
    {
        // SETUP
        $employee = $this->createEmployee();

        $this->entityManager->persist($employee);
        $this->entityManager->flush();

        // Fetch record and assert it was created
        $employeeRecord = $this->entityManager->getRepository(Employee::class)->find($employee->getId());

        $this->assertEquals('John Doe', $employeeRecord->getName());
        $this->assertEquals('john.doe@example.com', $employeeRecord->getEmail());
        $this->assertEquals('0412345678', $employeeRecord->getPhoneNumber());
        $this->assertEquals('/profiles/images/john.jpg', $employeeRecord->getProfileImage());
        $this->assertEquals(0, $employeeRecord->getRosteredHours());
        $this->assertEquals(38, $employeeRecord->getRosteredAllowance());

        // DO SOMETHING
        // Delete the record
        $this->entityManager->remove($employeeRecord);
        $this->entityManager->flush();

        // Query for non existing record
        $employeeRecord = $this->entityManager->getRepository(Employee::class)->findOneBy(['name' => 'John Doe']);

        // ASSERT
        $this->assertEmpty($employeeRecord);
    }

    /** @test */
    public function an_employees_attached_roles_can_be_retrieved(): void
    {
        // SETUP
        $role = new WorkRole();
        $role->setName('Bartender')
            ->setDescription('Front of House Bar Staff')
            ->setSlug('bartender');

        $employee = $this->createEmployee();
        $employee->addWorkRole($role);

        $this->entityManager->persist($employee);
        $this->entityManager->persist($role);
        $this->entityManager->flush();

        // DO SOMETHING
        $employeeRecord = $this->entityManager->getRepository(Employee::class)->findOneBy(['name' => 'John Doe']);

        // ASSERT
        $this->assertEquals('Bartender', $employeeRecord->getWorkRoles()[0]->getName());
    }

    /** @test */
    public function an_employees_attached_qualifications_can_be_retrieved(): void
    {
        // SETUP
        // Create Qualification
        $qualification = new Qualification();
        $awardedAt = new DateTime('2021-06-01');
        $qualification->setName('Bartender License')
            ->setDescription('License for Bartending')
            ->setAwardedAt($awardedAt);

        // Create Employee
        $employee = $this->createEmployee();
        $employee->addQualification($qualification);

        $this->entityManager->persist($qualification);
        $this->entityManager->persist($employee);
        $this->entityManager->flush();

        // DO SOMETHING
        // Retrieve Employee
        $employeeRecord = $this->entityManager->getRepository(Employee::class)->find($employee->getId());

        // ASSERT
        $this->assertEquals('Bartender License', $employeeRecord->getQualifications()[0]->getName());
    }

    /** @test */
    public function an_employees_attached_shifts_can_be_retrieved(): void
    {
        // SETUP
        // Create Shift
        $shift = new Shift();
        $shift_date = new DateTime('2020-12-01');
        $shift_start = new DateTime('2021-12-01 08:00');
        $shift_end = new DateTime('2021-12-01 15:00');
        $shift->setDate($shift_date)
            ->setStartTime($shift_start)
            ->setEndTime($shift_end);

        $this->entityManager->persist($shift);

        // Create Employee
        $employee = $this->createEmployee();
        $employee->addShift($shift);

        $this->entityManager->persist($employee);
        $this->entityManager->flush();

        // DO SOMETHING
        // Retrieve Employee
        $employeeRecord = $this->entityManager->getRepository(Employee::class)->find($employee->getId());

        // ASSERT
        $this->assertEquals($shift_start, $employeeRecord->getShifts()[0]->getStartTime());
    }

    /** @test */
    public function an_employees_leave_period_can_be_retrieved(): void
    {
        // SETUP
        // Create Leave
        $leave = new Leave();
        $startDate = new DateTime('2020-11-01');
        $endDate = new DateTime('2021-12-01');
        $leave->setType('Recreation')
            ->setStartDate($startDate)
            ->setEndDate($endDate)
            ->setNotes('Christmas Break leave');

        $this->entityManager->persist($leave);

        // Create Employee
        $employee = $this->createEmployee();
        $employee->addLeave($leave);

        $this->entityManager->persist($employee);
        $this->entityManager->flush();

        // DO SOMETHING
        // Retrieve Employee
        $employeeRecord = $this->entityManager->getRepository(Employee::class)->find($employee->getId());

        // ASSERT
        $this->assertEquals('Recreation', $employeeRecord->getLeave()[0]->getType());
    }
}
