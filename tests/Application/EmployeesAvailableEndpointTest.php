<?php

namespace App\Tests\Application;

use App\Tests\Feature\DatabasePrimer;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\AbstractDatabaseTool;
use App\DataFixtures\WorkAreaDemoFixtures;
use App\DataFixtures\WorkRoleDemoFixtures;
use App\DataFixtures\QualificationDemoFixtures;
use App\DataFixtures\EmployeeDemoFixtures;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EmployeesAvailableEndpointTest extends WebTestCase
{
    protected $databaseTool;

    protected $entityManager;

    public function setUp(): void
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
        $this->referenceRepository = new ReferenceRepository($this->entityManager);

        /** 
         * @todo Not working for Sqlite. 
         * See: https://github.com/liip/LiipTestFixturesBundle/issues/154
         * */
        // $this->databaseTool->loadFixtures([
        //     'App\DataFixtures\EmployeeDemoFixtures'
        // ]);

        $workAreaFixture = new WorkAreaDemoFixtures();
        $workAreaFixture->setReferenceRepository($this->referenceRepository);
        $qualificationFixture = new QualificationDemoFixtures();
        $qualificationFixture->setReferenceRepository($this->referenceRepository);
        $workRoleFixture = new WorkRoleDemoFixtures();
        $workRoleFixture->setReferenceRepository($this->referenceRepository);
        $employeeFixture = new EmployeeDemoFixtures();
        $employeeFixture->setReferenceRepository($this->referenceRepository);
        $workAreaFixture->load($this->entityManager);
        $qualificationFixture->load($this->entityManager);
        $workRoleFixture->load($this->entityManager);
        $employeeFixture->load($this->entityManager);

        self::ensureKernelShutdown();
    }

    /** @test */
    public function endpoint_receives_valid_json_response(): void
    {
        // SETUP
        $client = static::createClient();
        $client->request('GET', '/api/employees/available');
        $response = $client->getResponse();

        // ASSERT
        $this->assertResponseIsSuccessful();
        $this->assertJson($response->getContent());
    }

    /** @test */
    public function endpoint_returns_two_lists_of_employees(): void
    {
        // SETUP
        $client = static::createClient();
        $client->request('GET', '/api/employees/available');
        $response = $client->getResponse();
        $responseData = json_decode($response->getContent());

        // ASSERT
        $this->assertResponseIsSuccessful();
        $this->assertJson($response->getContent());
        $this->assertObjectHasAttribute('employees_best_match', $responseData);
        $this->assertObjectHasAttribute('employees_other_staff', $responseData);
    }

    /** @test */
    public function best_match_employees_match_shift_role(): void
    {
        // SETUP
        $client = static::createClient();

        $client->request('GET', '/api/employees/available?workrole=bartender&area=bar-main');
        $response = $client->getResponse();
        $responseData = json_decode($response->getContent());

        // ASSERT
        $this->assertResponseIsSuccessful();
        $this->assertJson($response->getContent());
        $this->assertObjectHasAttribute('slug', $responseData->employees_best_match[0]->workRoles[0]);
        $this->assertEquals('bartender', $responseData->employees_best_match[0]->workRoles[0]->slug);
    }

    /** @test */
    public function other_staff_employees_match_area(): void
    {
        // SETUP
        $client = static::createClient();
        $client->request('GET', '/api/employees/available?workrole=bartender&area=bar-main');
        $response = $client->getResponse();
        $responseData = json_decode($response->getContent());

        // ASSERT
        $this->assertResponseIsSuccessful();
        $this->assertJson($response->getContent());
        $this->assertObjectHasAttribute('slug', $responseData->employees_other_staff[0]->workRoles[0]);
        $this->assertEquals('manager', $responseData->employees_other_staff[0]->workRoles[0]->slug);
    }

    /** @test */
    public function best_match_employees_are_sorted_by_hours_and_rating(): void
    {
        // SETUP
        $client = static::createClient();
        $client->request('GET', '/api/employees/available');
        $response = $client->getResponse();

        // ASSERT
        $this->assertResponseIsSuccessful();
        $this->assertJson($response->getContent());
    }
}
