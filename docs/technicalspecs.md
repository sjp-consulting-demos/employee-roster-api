# Overview
Create a prototype of an API that will return a list of employees available for a given shift at a given work location. A mockup has been given showing a user creating a new shift, and when clicking 'select an employee', a dropdown menu appears with two sections. The first section should show a list of "best match" employees in order of rating, and the second section should show a list of "other staff" employees who may be suitable for the shift but aren't directly associated with the role.

# Requirements
Write an API endpoint to provide the list of employees.

The response should provide a list of **best matches** and **other matches**:

* For a specific role
* In a specific location
* On a specific day
* At a specific time
* For a specific length of shift
* Where those employees are available and able to work.

## Definition of Done
There **must** be an endpoint which provides a list of **best matches** and **other matches** in JSON format which can be consumed by a frontend.

All Feature Tests in PHPUnit should pass.

## Timeframe
1 week

# Solution
## Technical specifications
The solution will be built with the following:
* Symfony 5
* PHP 8
* SqLite
* MySql
* Docker

The final demonstration product can be tested with Postman once the Docker containers are running

The following testsuites are included:
* Feature tests
* Application tests

## API specifications
[API Design](apidesign.md)

## Database Design
[Database Design](databasedesign.md)

## Testing Strategy
[Testing Strategy](testingstrategy.md)

## Test Results
[Test Results](testresults.html)