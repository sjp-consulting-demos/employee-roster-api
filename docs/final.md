# Final Thoughts

## Issues that came up
* Issue with Liip Test Fixtures library not currently working with Sqlite
    * Ran fixtures "manually" by instantiating required classes instead
* Fixtures did not work as expected in live dev environment after phpUnit tests
    * Entity relationships were not mapping correctly
    * Some changes and updates were made to Entities which broke the relationships but not picked up in PhpUnit tests
* Factory issues: These weren't working with the Fixtures initially. There is probably a better way to use them as well.
* Entity relation issues: Lazy loading caused some trouble with empty fields in the response (fixed)
* Time constraints - spent a bit too much time investigating different issues
    * Not all tests were created as we zoned in on the 'must-haves'

## Future State
* Implement star (1-5) rating system
* Better filtering
* Refactor the `EmployeeRosterManager` to be more modular
* Implement missing tests