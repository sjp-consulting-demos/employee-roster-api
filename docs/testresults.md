Employees Available Endpoint (App\Tests\Application\EmployeesAvailableEndpoint)
- [x] Endpoint receives valid json response
- [x] Endpoint returns two lists of employees
- [x] Best match employees match shift role
- [x] Other staff employees match area
- [x] Best match employees are sorted by hours and rating

Demo Fixtures (App\Tests\Feature\DemoFixtures)
- [x] Area fixtures create demo areas
- [x] Work role fixtures create demo work roles
- [x] Qualification fixtures create demo qualifications
- [x] Employee fixtures create demo employees
- [x] Leave fixtures create demo fixtures
- [ ] Shift fixtures create demo shifts

Area (App\Tests\Feature\Entities\Area)
- [x] An area record can be created in the database
- [x] An area record can be updated in the database
- [x] An area record can be deleted in the database

Employee (App\Tests\Feature\Entities\Employee)
- [x] An employee record can be created in the database
- [x] An employee record can be updated in the database
- [x] An employee record can be deleted in the database
- [x] An employees attached roles can be retrieved
- [x] An employees attached qualifications can be retrieved
- [x] An employees attached shifts can be retrieved
- [x] An employees leave period can be retrieved

Leave (App\Tests\Feature\Entities\Leave)
- [x] A leave record can be created in the database
- [x] A leave record can be updated in the database
- [x] A leave record can be deleted in the database
- [x] An employee can be attached and retrieved

Qualification (App\Tests\Feature\Entities\Qualification)
- [x] A qualification record can be created in the database
- [x] A qualification record can be updated in the database
- [x] A qualification record can be deleted in the database
- [x] Employees can be attached and retrieved from a qualification
- [x] An employee can be detached from a qualification

Role (App\Tests\Feature\Entities\Role)
- [x] A work role record can be created in the database
- [x] A work role record can be updated in the database
- [x] A work role record can be deleted in the database
- [x] An employee can be attached to a work role
- [x] An employee can be detached from a work role
- [x] A work area can be attached to a work role
- [x] A work area can be detached from a work role

Shift (App\Tests\Feature\Entities\Shift)
- [x] A shift record can be created in the database
- [x] A shift record can be updated in the database
- [x] A shift record can be deleted in the database

Employee Factory (App\Tests\Feature\Factories\EmployeeFactory)
- [ ] Placeholder

