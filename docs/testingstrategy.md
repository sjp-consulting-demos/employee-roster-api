# Testing Strategy
Testing will be mainly driven by automation through PHPUnit, which will handle all Unit, Feature, and Application Tests.

* Feature testing - Testing individual classes and functionality
* Application testing - Testing the application stack by sending requests to the API

Feature testing will be required for testing basic functionality on the `Entities` and `Repositories` - these will be database dependent tests

Application Tests will make different API calls testing different parameters of the API and checking the results. This will make use of an `HttpClient` and the `GET api/employees/available` endpoint.

## Unit Tests
Entities to test:
* `Employee`
	* Can be created with valid data
	* It creates with the correct relationships
	* Does not create with invalid data - throws error
* `Role`
	* Can be created with valid data
	* Does not create with invalid data - throws error
* `Qualification`
	* Can be created with valid data
	* Does not create with invalid data - throws error
* `Shift`
	* Can be created with valid data
	* Does not create with invalid data - throws error
* `EmployeeRepository`
	* Can retrieve employee with associated `roles`, `qualifications`, and current `shifts`, including `rostered_allowance` representing available hours and `rostered_hours` representing the total number of assigned hours across all shifts for the employee

## Feature Tests
Testing the `GET /employees/available` endpoint with:
* Retrieve list of employees for `shift` with `role`
	* Should show all employees for all `locations` matching `role` and not already having a shift in the `shift` timeslot
	* Should not display any employees with current `leave`, or those with no `rostered_allowance` hours available
* Retrieve employees for `shift` with `qualification`
	* Should show all employees for all `locations` matching `qualification` and not already having a shift in the `shift` timeslot
	* Should not display any employees with current `leave`, or those with no `rostered_allowance` hours available
* Retrieve employees for [shift] at [location] with [role]
	* Should show all employees for `location` and matching `role`, and not already having a shift in the `shift` timeslot.
	* Should not display any employees with current `leave`, or those with no `rostered_allowance` hours available
* The retrieved employees data should be an `object` containing two `arrays` of employees - one array representing `best_match` and the other representing `other_employees`

### Command Interface - Should Have
There should be some utility commands to make running things and testing things easier, including user creation and shift creation, to replicate the functionality that a front-end could consume.

This is a 'should have' requirement due to potential time constraints implementing all the commands. It is important to focus on the API endpoint primarily as the commands are intended to be utility helpers when engaging in general development.

## Test Data
Test data for entities can utilise `zenstruck/foundry` bundle to make use of factories to speed up entity testing. This also includes a wrapper for `faker` which can be used to generate random data. `dama/docrtine-test-bundle` is also utilised to create more efficient tests and database transactions to reduce testing time.

Test data can otherwise be created manually for manual testing and general development using the above utility commands via `symfony console`