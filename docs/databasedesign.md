# Database Design

The following schema is given in the technical specs:

* **Areas** - A work location
* **Employees** - name, profile picture, work location & role
* **EmployeeRoles** - Employee's roles they can work, as well as proficiency
* **EmployeeQualifications** - Employee's qualifications, both active & inactive
* **Leave** - Days employees will be taking off
* **Roles** - Role within a work location
* **RoleQualifications** - Qualifications required to work within a role
* **Shifts** - start date & time, end date & time, related role, related employee (optional).
* **Qualifications** - Definition of a qualification


# Schema Diagram
![Architecture-Data_ER.png](img/data_er.png)